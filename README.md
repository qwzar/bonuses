#  Ledger - double-entry accounting

Доклад для дринкапа
https://docs.google.com/presentation/d/11IpvUYUzJ-rsbM3-8i06KBRRLW_BDDJGyIT9-4Va-No/edit?usp=sharing

## Run

```bash
$ export FLASK_APP=webapp.py
$ flask init
$ flask run --host=0.0.0.0
```  

## Example feed
```
$ make seed
```


## Author
forked from [gregnavis/ledger](https://github.com/gregnavis/ledger).